##
## step-1-configure.sh
##
## This script is meant to be run as a regular user.
##

# Upgrade all outdated packages
sudo pacman -Syu --noconfirm

# Install misc essential apps
sudo pacman -S --noconfirm base-devel dosfstools exfat-utils networkmanager ntfs-3g
sudo systemctl enable NetworkManager

# Make dirs
sudo mkdir -p /mnt/{storage,thumb,tmp} /mnt/nas/{music,pictures,videos,other}

# Edit fstab
echo '# NAS' | sudo tee -a /mnt/etc/fstab
echo 'hurray:/mnt/nas/music    /mnt/nas/music    nfs defaults,noauto,users,vers=4 0 0' | sudo tee -a /mnt/etc/fstab
echo 'hurray:/mnt/nas/other    /mnt/nas/other    nfs defaults,noauto,users,vers=4 0 0' | sudo tee -a /mnt/etc/fstab
echo 'hurray:/mnt/nas/pictures /mnt/nas/pictures nfs defaults,noauto,users,vers=4 0 0' | sudo tee -a /mnt/etc/fstab
echo 'hurray:/mnt/nas/videos   /mnt/nas/videos   nfs defaults,noauto,users,vers=4 0 0' | sudo tee -a /mnt/etc/fstab

# Set locale, time
echo 'en_US.UTF-8 UTF-8' | sudo tee -a /etc/locale.gen
sudo locale-gen
echo 'LANG=en_US.UTF-8' | sudo tee /etc/locale.conf
sudo ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
sudo hwclock --systohc

# Set hostname, hosts
echo "goofball" | sudo tee /etc/hostname
sudo cp /pinebook-pro-arch-linux/assets/hosts /etc/hosts

# Partitions, folders
sudo mkdir /opt/aur
sudo mkdir -p /mnt/storage/{books,non-dev_work,school}
sudo mkdir -p /mnt/storage/dev/{dev-current,dev-archived-selected}
sudo mkdir -p /mnt/storage/downloads/os_images/
sudo mkdir -p /mnt/storage/downloads/torrents/{in,}complete
sudo mkdir -p /mnt/storage/envs/{docker,go,node,python,vagrant,virtualbox}
sudo mkdir -p /mnt/storage/music/{music-not_ready-selected,music-mom,music-ready}
sudo chown ian:ian -R /mnt/* /opt/aur
ln -s -t ~ /mnt/storage
ln -s -t ~ /mnt/storage/dev
ln -s -t ~ /mnt/storage/non-dev_work
ln -s -t ~ /mnt/storage/downloads
ln -s -t ~ /mnt/storage/music

# Basic dev tools
sudo pacman -S --noconfirm alacritty ranger openssh bash-completion man-pages
git config --global user.email "ian.s.mcb@gmail.com"
git config --global user.name "Ian S. McBride"
git config --global user.username "ian-s-mcb"
git config --global init.defaultBranch "main"
git config --global pull.rebase "false"
git clone https://gitlab.com/ian-s-mcb/pinebook-pro-arch-linux.git ~/dev/dev-current/pinebook-pro-arch-linux
git clone https://gitlab.com/ian-s-mcb/scripts.git ~/dev/dev-current/scripts
git clone https://gitlab.com/ian-s-mcb/dotfiles.git ~/dev/dev-current/dotfiles
ln -s -t ~ ~/dev/dev-current/pinebook-pro-arch-linux
ln -s -T ~/dev/dev-current/dotfiles ~/.config
# Add sym links for dotfiles that need to reside outside of ~/.config
rm ~/.bash{rc,_logout,_profile}
mkdir ~/.ssh
ln -s -T ~/.config/.ssh_config ~/.ssh/config
ln -s -t ~ ~/.config/.bash_logout
ln -s -t ~ ~/.config/.bash_profile
ln -s -t ~ ~/.config/.bashrc
ln -s -t ~ ~/.config/.dircolors
ln -s -t ~ ~/.config/.vimrc
ln -s -t ~ ~/.config/.pam_environment
systemctl enable ssh-agent.service --user
systemctl start ssh-agent.service --user
sudo systemctl enable sshd.service
sudo systemctl start sshd.service

# Fonts
sudo pacman -S --noconfirm terminus-font ttf-dejavu noto-fonts-emoji noto-fonts-cjk
echo 'FONT=ter-v18n' | sudo tee /etc/vconsole.conf
git clone https://aur.archlinux.org/nerd-fonts-fira-code /opt/aur/nerd-fonts-fira-code
cd /opt/aur/nerd-fonts-fira-code
makepkg --noconfirm -si

# Tmux
sudo pacman -S --noconfirm tmux
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
ln -s -t ~ ~/.config/.tmux.conf
mkdir ~/.tmuxp
ln -s -t ~/.tmuxp ~/.config/base.yaml
sudo pacman -S --noconfirm tmuxp

# Vim
mkdir -p ~/.vim/pack/my-packages/{opt,start}
git clone https://github.com/fnune/base16-vim.git ~/.vim/pack/my-packages/opt/base16-vim
git clone https://github.com/tpope/vim-vinegar.git ~/.vim/pack/my-packages/start/vim-vinegar

# Window manager and friends
sudo pacman -S --noconfirm sway xorg-xwayland waybar wf-recorder wl-clipboard mako
sudo pacman -S --noconfirm nm-connection-editor rofi grim slurp light swaylock swayidle pipewire xdg-desktop-portal-wlr network-manager-applet
systemctl enable pipewire.service --user
systemctl start pipewire.service --user
systemctl enable xdg-desktop-portal-wlr.service --user
systemctl start xdg-desktop-portal-wlr.service --user

# Audio
sudo pacman -S --noconfirm pulseaudio pavucontrol pulseaudio-alsa pulseaudio-bluetooth pulseaudio-jack pulsemixer alsa-utils bluez bluez-utils blueman beets gst-plugins-ugly soundconverter
sudo systemctl enable bluetooth.service
sudo systemctl start bluetooth.service

# Stig / transmission-daemon
sudo pacman -S --noconfirm transmission-cli
# Add static copy of dotfile (to avoid ln causing the loss of strict file permissions)
sudo mkdir -p /var/lib/transmission/.config/transmission-daemon/
sudo cp ~/.config/transmission-daemon/settings.json /var/lib/transmission/.config/transmission-daemon/
sudo chown transmission:transmission -R /var/lib/transmission
sudo chown ian:transmission /mnt/storage/downloads/torrents/{in,}complete
chmod 770 /mnt/storage/downloads/torrents/{in,}complete
sudo systemctl start transmission
sudo systemctl enable transmission
git clone https://aur.archlinux.org/stig.git /opt/aur/stig
cd /opt/aur/stig
makepkg --noconfirm -si

# Web broswers
sudo pacman -S --noconfirm firefox chromium

# Mpd, mpc, ncmpcpp
git clone https://gitlab.com/ian-s-mcb/song-picks.git ~/music/music-song-picks
sudo pacman -S --noconfirm mpd mpc ncmpcpp
ln -s -t ~/.config/mpd/music ~/music
mkdir ~/.config/mpd/playlists
touch ~/.config/mpd/state
systemctl enable mpd --user
systemctl start mpd --user

# Misc OS config
sudo pacman -S --noconfirm nfs-utils # For NAS
echo 'HandleLidSwitch=ignore' | sudo tee -a /etc/systemd/logind.conf # For disabling suspend on laptop lid close
sudo pacman -S --noconfirm ntp
sudo systemctl enable ntpd.service
sudo systemctl start ntpd.service

# Misc apps
sudo pacman -S --noconfirm foliate neofetch
sudo pacman -S --noconfirm gimp mpv imv zip unzip unrar p7zip gucharmap curl wget img2pdf man parted ethtool wol

# Python
sudo pacman -S --noconfirm python-pip glibc
sudo pip install virtualenvwrapper
export WORKON_HOME=/mnt/storage/envs/python
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source /usr/bin/virtualenvwrapper.sh
mkvirtualenv base
pip install -r ~/dev/dev-current/scripts/base_python_env.txt
deactivate
# For future loading use alias `py`

# Node.js
git clone https://aur.archlinux.org/nvm.git /opt/aur/nvm
cd /opt/aur/nvm
makepkg --noconfirm -si
mkdir -p /mnt/storage/envs/node/yarn-global/{cache,lib}
export NVM_DIR="/mnt/storage/envs/node"
source /usr/share/nvm/nvm.sh
source /usr/share/nvm/bash_completion
source /usr/share/nvm/install-nvm-exec
nvm install --lts
nvm use --lts
npm i -g yarn
yarn config set prefix /mnt/storage/envs/node/yarn-global/lib
yarn config set cache-folder /mnt/storage/envs/node/yarn-global/cache
yarn config set global-folder /mnt/storage/envs/node/yarn-global/lib
# For future loading use alias `nd`

# Docker
sudo pacman -S --noconfirm docker
sudo usermod -a -G docker ian
sudo mkdir /etc/docker
echo -e "{\n  \"data-root\": \"/mnt/storage/envs/docker\"\n}" | sudo tee /etc/docker/daemon.json
