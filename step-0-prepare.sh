##
## step-0-prepare.sh
##
## A guide for starting the configuration script. Be advised that this
## file is not meant for automated execution. Instead, it is meant for
## reading purposes as you execute the instructions by hand.
##

# Connect to Wi-Fi
wifi-menu

# Install apps needed for step-1
pacman -Sy sudo vim

# Clone repo with installer scripts
git clone https://gitlab.com/ian-s-mcb/pinebook-pro-arch-linux.git /pinebook-pro-arch-linux

# Mount storage partition
#
# * NOTE: this assumes you created the partition while you were
#   following SvenKiljan's PBP Arch Linux instructions.
# * Having a storage partition for auxillary, non-OS files is a
#   preference of mine
# * This partition makes it easier to reinstall the OS in the future,
#   while keeping the auxillary files intact
mkdir -p /mnt/storage
echo 'LABEL=STORAGE /mnt/storage ext4 defaults 0 0' >> /etc/fstab
mount -a

# Setup user
echo '%sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
groupadd -r sudo
useradd -b /home -G sudo,video -m ian
echo 'ian:ian' | chpasswd ian
echo 'root:root' | chpasswd root

# Login as user
su ian

# Run installer script
bash -x /pinebook-pro-arch-linux/step-1-configure.sh
