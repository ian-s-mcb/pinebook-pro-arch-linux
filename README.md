# pinebook-pro-arch-linux

A script that configures to my liking a [Pinebook Pro][pbp-product-page]
workstation with [Arch Linux][arch].

## How to use this script

* Install Arch Linux
  * The [community recommended approach][pbp-wiki-install] is to use the
    disk image and instructions from community member
    [Sven Kiljan][sven], which you can find [here][sven-pbp-installer].
  * The instructions vary depending on the target device for your OS
    (ie. SD card, eMMC, or NVMe). In my case, the target device was an
    NVMe.
  * **NOTE:** While following the instructions, create a storage
    partition on your target device (in addition to the root partion)
    and give it the label "STORAGE". The storage partition is a
    necessary part of the final workstation and the following steps
    assume that it exists.
* Boot into newly installed OS
* Manually run the prepare commands in `step-0-prepare.sh`
* Run the configure script

  `bash -x /pinebook-pro-arch-linux/step-1-configure.sh`
* Manually run the cleanup commands in `step-2-cleanup.sh`

## Acknowledgments

* [PINE64][pine64], for providing affordable, community-centric hardware
* [Sven Kiljan][sven], for creating and maintaining an Arch Linux build
  for the Pinebook Pro
* [Another Arch Linux script of mine][ian-x86-installer], but for
  x86_64, that served as the basis for this script

[arch]: https://en.wikipedia.org/wiki/Arch_Linux
[ian-x86-installer]: https://gitlab.com/ian-s-mcb/arch-linux
[pbp-product-page]: https://www.pine64.org/pinebook-pro/
[pbp-wiki-install]: https://wiki.pine64.org/wiki/Pinebook_Pro_Software_Release#Arch_Linux_ARM
[pine64]: https://www.pine64.org/
[sven]: https://github.com/SvenKiljan
[sven-pbp-installer]: https://github.com/SvenKiljan/archlinuxarm-pbp
