##
## step-2-manual_cleanup.sh
##
## This script addresses the necessary, non-automated clean needed after
## installing the OS. This script is meant for reading purposes, not
## automated execution. Follow along with the instructions in this file.
##

# SSH key
cp -r /path/to/ssh/folder/with/keys ~/.ssh

# Remove old copy of installer repo
sudo rm -r /pinebook-pro-arch-linux

# Replace repo remotes with ones that use ssh keys
cd ~/dev/dev-current/pinebook-pro-arch-linux
git remote set-url origin git@gitlab.com:ian-s-mcb/pinebook-pro-arch-linux.git
cd ~/dev/dev-current/dotfiles
git remote set-url origin git@gitlab.com:ian-s-mcb/dotfiles.git
cd ~/dev/dev-current/scripts
git remote set-url origin git@gitlab.com:ian-s-mcb/scripts.git
cd ~/music/music-song-picks
git remote set-url origin git@gitlab.com:ian-s-mcb/song-picks.git

# Tmux
tmux
# Type `Ctrl-b I` to download plugins

# Bluetooth
# Enable bluetooth on boot
# Edit /etc/bluetooth/main.conf
# Add
#     AutoEnable=true
# to the group
#     [Policy]

# Gimp
# Follow instructions to add arrow tool
# https://progsharing.blogspot.com/2018/06/draw-arrows-with-gimp-plugins.html#arrow_scm_branger
sudo cp ~/downloads/arrow.scm /usr/share/gimp/2.0/scripts/

# Users
sudo passwd ian # Set a real password for ian
sudo visudo # Remove passwordless sudo for members of sudo group
sudo passwd -l root # Lock root user

# Firefox - torrent magnet link handling
# Navigate to about:config
# Add a boolean
#    name: network.protocol-handler.expose.magnet
#    value: false
# Look up a magnet link, click it, associate scripts/add_magnet.sh to
# the type of link
